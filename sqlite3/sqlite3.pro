TARGET   = sqlite3
TEMPLATE = lib
#DESTDIR  = $$PWD/../bin
CONFIG(debug, debug|release) {
DESTDIR  = $$PWD/../debug
} else {
DESTDIR  = $$PWD/../release
}
#message($$PWD)

msvc {
    DEFINES += SQLITE_ENABLE_COLUMN_METADATA
    DEF_FILE = sqlite3.def
    QMAKE_CFLAGS += /utf-8
    QMAKE_CXXFLAGS += /utf-8
}

macx
{
    # 编译时候指定libs查找位置
    #QMAKE_LFLAGS_RELEASE += -Wl,-rpath,$$PWD/../../../Release/libs -Wl
    #QMAKE_LFLAGS_DEBUG += -Wl,-rpath,$$PWD/../../../Release/libs -Wl
    QMAKE_LFLAGS_SONAME = -Wl,-install_name,@rpath/
    #指定生成路径
    CONFIG(debug, debug|release){
        DESTDIR = $$PWD/../Debug/SQLiteExplorer.app/Contents/MacOS
    } else {
        DESTDIR = $$PWD/../Release/SQLiteExplorer.app/Contents/MacOS
    }
}

HEADERS = sqlite3.h

SOURCES = sqlite3.c

TRANSLATIONS += $$PWD/../SQLiteExplorer/rc/translations/zh.ts
