#ifndef QSQLITEQUERYWINDOW_H
#define QSQLITEQUERYWINDOW_H

#include <QWidget>
#include <QSplitter>
#include "highlighter.h"

namespace Ui {
class QSQLiteQueryWindow;
}

class QSQLiteTableView;
class MainWindow;

class QSQLiteQueryWindow : public QWidget
{
    Q_OBJECT

public:
    explicit QSQLiteQueryWindow(QWidget *parent = 0);
    ~QSQLiteQueryWindow();

signals:
    void signalSQLiteQuery(const QString& sql);
    void authTriggerd(int nCode, QString strCode,
                      QString s1, QString s2, QString s3, QString s4);

private slots:
    void onExecuteBtnClicked();
    void onExplainBtnClicked();
    void onDataLoaded(const QString& msg);
private:
    Ui::QSQLiteQueryWindow *ui;

    QSQLiteTableView* m_pTableView;

    // QSplitter
    QSplitter* m_pSplitter;

    MainWindow* m_pParent;
    Highlighter* m_pHighLighter;
};

#endif // QSQLITEQUERYWINDOW_H
